package com.sanjit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringapiwarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringapiwarApplication.class, args);
	}

}
