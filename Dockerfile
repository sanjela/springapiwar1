FROM tomcat:8.0-alpine

RUN rm -rf /usr/local/tomcat/webapps/*
ADD ./target/springapiwar-0.0.1-SNAPSHOT.war  /usr/local/tomcat/webapps/ROOT.war


CMD ["catalina.sh", "run"]